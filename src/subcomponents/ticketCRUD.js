import React, { Component } from 'react';

import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

class ticketCRUD extends Component {

    render() {

        const products = this.props.products;
        const select = this.props.selectAction;
        const deleteAction = this.props.deleteAction;

        return (


            <table>
                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>ID</TableHeaderColumn>
                        <TableHeaderColumn>Name</TableHeaderColumn>
                        <TableHeaderColumn>Email</TableHeaderColumn>
                        <TableHeaderColumn>Phone</TableHeaderColumn>
                        <TableHeaderColumn>Operating System</TableHeaderColumn>
                        <TableHeaderColumn>Type</TableHeaderColumn>
                        <TableHeaderColumn>Description</TableHeaderColumn>
                    </TableRow>
                </TableHeader>

                {products.map( function(product, i) {
                    return (
                        <TableRow key={i}>
                            <TableRowColumn>{product.id}</TableRowColumn>
                            <TableRowColumn>{product.name}</TableRowColumn>
                            <TableRowColumn>{product.email}</TableRowColumn>
                            <TableRowColumn>{product.phone}</TableRowColumn>
                            <TableRowColumn>{product.opsystype}</TableRowColumn>
                            <TableRowColumn>{product.type}</TableRowColumn>
                            <TableRowColumn>{product.desc}</TableRowColumn>

                            <TableRowColumn><button onClick={() => select(i)}>Update</button></TableRowColumn>
                            <TableRowColumn><button onClick={() => deleteAction(product.id)}>Delete</button></TableRowColumn>
                        </TableRow>
                    )
                })}
            </table>
        )
    }

}

export default ticketCRUD