import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import injectTapEventPlugin from "react-tap-event-plugin";
import {Redirect, Route, Router} from "react-router";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import createBrowserHistory from "history/createBrowserHistory";
import Login from "./features/Login";
import Home from "./features/Home";
import App from './App';
import firebase from 'firebase';
import {BrowserRouter} from "react-router-dom";

const muiTheme = getMuiTheme({
    appBar: {
        color: "#37517E",
        height: 50
    },
});

const config = {
    apiKey: "AIzaSyDbkfhUSiMOVDohZJlT_Fdbal-LYKXRzdY",
    authDomain: "assignment2-f2b7c.firebaseapp.com",
    databaseURL: "https://assignment2-f2b7c.firebaseio.com",
    projectId: "assignment2-f2b7c",
    storageBucket: "assignment2-f2b7c.appspot.com",
    messagingSenderId: "1011925878703"
};
firebase.initializeApp(config);

injectTapEventPlugin();

const customHistory = createBrowserHistory();
const Root = () => (
    <MuiThemeProvider muiTheme={muiTheme}>
        <Router history={customHistory}>
            <div>
                <Route path="/login" component={Login}/>
                <Route path="/app/home" component={Home}/>
                <Redirect from="/" to="/login"/>
            </div>
        </Router>
    </MuiThemeProvider>

);
ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
    , document.getElementById('root'));
registerServiceWorker();



